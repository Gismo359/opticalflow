#pragma once

#include "Algorithm.hpp"

template <typename ValueType, typename SizeType = int>
struct Vector
{
public:
    // TODO@Daniel:
    // Iterator type aliases
private:
    ValueType *data_;
    SizeType size_;
    SizeType capacity_;

    void Reallocate(SizeType capacity)
    {
        ValueType *new_data = (ValueType*)(::operator new(capacity * alignof(ValueType)));

        if (data_ != nullptr)
        {
            Copy(
                data_,
                data_ + Min(size_, capacity),
                new_data
            );

            delete data_;
        }

        data_ = new_data;
        capacity_ = capacity;
    }

    void Grow(SizeType target_capacity)
    {
        SizeType current_capacity = Capacity();
        SizeType new_capacity;
        if (current_capacity == 0)
        {
            new_capacity = target_capacity;
        }
        else
        {
            new_capacity = 2 * current_capacity * (1 + target_capacity / current_capacity);
        }
        Reallocate(new_capacity);
    }

public:
    Vector() :
        data_(nullptr),
        size_(0),
        capacity_(0)
    {

    }

    Vector(SizeType size)
    {
        Resize(size);
    }

    ValueType const *Data() const
    {
        return data_;
    }

    ValueType *Data()
    {
        return data_;
    }

    SizeType Size() const
    {
        return size_;
    }

    SizeType Capacity() const
    {
        return capacity_;
    }

    void Reserve(SizeType capacity)
    {
        if (capacity > Capacity())
        {
            Grow(capacity);
        }
    }

    void Resize(SizeType size)
    {
        Reserve(size);
        size_ = size;
    }

    void ShrinkToFit()
    {
        Reallocate(Size());
    }

    ValueType const &operator[](int idx) const
    {
        return data_[idx];
    }

    ValueType &operator[](int idx)
    {
        return data_[idx];
    }

    ValueType const &At(int idx) const
    {
        return data_[idx];
    }

    ValueType &At(int idx)
    {
        return data_[idx];
    }

    void Append(ValueType const &value)
    {
        SizeType size = Size();
        Reserve(size + 1);
        At(size) = value;
        Resize(size + 1);
    }

    void Clear()
    {
        Resize(0);
    }
};